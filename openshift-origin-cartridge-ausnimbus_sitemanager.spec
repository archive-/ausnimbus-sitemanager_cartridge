%global cartridgedir %{_libexecdir}/openshift/cartridges/ausnimbus_sitemanager

Summary:       AusNimbus Site Manager Cartridge support for OpenShift
Name:          openshift-origin-cartridge-ausnimbus_sitemanager
Version:       1.0
Release:       1%{?dist}
Group:         Applications/Internet
License:       ASL 2.0
URL:           https://www.ausnimbus.com.au
Source0:       https://bitbucket.org/ausnimbus/ausnimbus_filemanager_cartridge/get/master.zip
Requires:      rubygem(openshift-origin-node)
Requires:      openshift-origin-node-util
Provides:      openshift-origin-cartridge-ausnimbus_sitemanager = 2.0.0
BuildArch:     noarch

%description
This cartridge provides embedded support in the effort to provide a simple GUI based file manager for files and database management. Recommended only for users who do not have access to/or want to use git, but still want to reap the benefits of the openshift platform. (Cartridge Format V2)

%prep
%setup -q

%build
%__rm %{name}.spec

%install
%__mkdir -p %{buildroot}%{cartridgedir}
%__cp -r * %{buildroot}%{cartridgedir}
%__mkdir -p %{buildroot}%{httpdconfdir}

%files
%dir %{cartridgedir}
%attr(0755,-,-) %{cartridgedir}/bin/
%{cartridgedir}/metadata
%{cartridgedir}/usr
%{cartridgedir}/versions
%{cartridgedir}/env
%doc %{cartridgedir}/README.md
%doc %{cartridgedir}/COPYRIGHT
%doc %{cartridgedir}/LICENSE

%changelog
* Mon May 25 2015 Andrew Lau <andrew.lau@ausnimbus.com.au> 1.0
- Initial version
