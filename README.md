# AusNimbus OpenShift FileManager Cartridge
This cartridge provides embedded support in the effort to provide a simple GUI based file manager for files and database management. Recommended only for users who do not have access to/or want to use git, but still want to reap the benefits of the openshift platform.
