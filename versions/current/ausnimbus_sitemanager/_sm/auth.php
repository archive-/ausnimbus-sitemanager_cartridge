<?php

# Reverse proxy only
if (empty($_SERVER['HTTP_X_REMOTE_USER'])) { echo "No access"; die; }

$users = trim(file_get_contents($_SERVER['OPENSHIFT_HOMEDIR'].".env/user_vars/AUSNIMBUS_SM_USER"));
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

if (empty($users) && empty($_POST)) {

$header = "Account Setup - put in your access key to get started";
include('header.php');
echo '
				<div class="form-group">
	        <form method="post" action="">
	                Access Key: <input type="password" name="accesskey" class="form-control"><br />
									<input class="btn btn-primary create" type="submit" value="Submit" style="float: right;">
	        </form>
				</div>
        ';
	die;

} else if (empty($users) && $_POST) {
        # User input form
        $accesskey = trim(file_get_contents($_SERVER['OPENSHIFT_HOMEDIR'].".env/user_vars/AUSNIMBUS_SM_KEY"));
        if ($_POST['accesskey'] == $accesskey)
        {
                file_put_contents($_SERVER['OPENSHIFT_HOMEDIR'].".env/user_vars/AUSNIMBUS_SM_USER",$_SERVER['HTTP_X_REMOTE_USER']);
                header("Refresh:1");
        } else {

$header = "Account Setup - put in your access key to get started";
include('header.php');
echo '
								<div class="alert alert-danger"><strong>Wrong access key...</strong></div>
								<div class="form-group">
					        <form method="post" action="">
					                Access Key: <input type="password" name="accesskey" class="form-control"><br />
													<input class="btn btn-primary create" type="submit" value="Submit" style="float: right;">
					        </form>
								</div>
                ';
                die;
        }
}

if ($_SERVER['HTTP_X_REMOTE_USER'] != $users) { echo "No access.."; die; }

?>
