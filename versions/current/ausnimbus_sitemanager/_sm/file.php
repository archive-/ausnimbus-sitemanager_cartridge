<?php
	$header = 'File Manager';
	include('header.php');
?>

			<script type="text/javascript">
				$().ready(function() {
					var elf = $('#elfinder').elfinder({
						url : '?f=connector',
						height: 400,
						uiOptions: {
								toolbar : [
										// toolbar configuration
										['back', 'forward'],
										['reload'],
										['home', 'up'],
										['mkdir', 'mkfile', 'download', 'upload'],
										['info'],
										['quicklook'],
										['copy', 'cut', 'paste'],
										['rm'],
										['duplicate', 'rename', 'edit'],
										['extract', 'archive'],
										['search'],
										['view']
								]
						},
						// Disable open
						// getFileCallback : function(files, fm) {
						// 	return false;
						// },
						// hooks
						handlers : {
							change : function(event, elfinderInstance) { GitStatus(); },
							add : function(event, elfinderInstance) { GitStatus(); },
							remove : function(event, elfinderInstance) { GitStatus(); },
							upload : function(event, elfinderInstance) { GitStatus(); },
							rename : function(event, elfinderInstance) { GitStatus(); },
							duplicate : function(event, elfinderInstance) { GitStatus(); }
						},
						contextmenu : {
								files  : [
										'getfile', 'open', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
										'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info'
								]
						},
						commandsOptions : {
					    edit : {
					      mimes : ['text/plain'], //types to edit
					      editors : [
					        {
					          mimes : ['text/plain'],  //types to edit with tinyMCE
					          load : function(textarea) {
					            tinymce.execCommand('mceAddEditor', false, textarea.id);
					          },
					          close : function(textarea, instance) {
					            tinymce.execCommand('mceRemoveEditor', false, textarea.id);
					          },
					          save : function(textarea, editor) {
					            textarea.value = tinyMCE.get(textarea.id).selection.getContent({format : 'raw'});
					            tinymce.execCommand('mceRemoveEditor', false, textarea.id);
					          }
					        },
					      ]
					    }
					  }

					}).elfinder('instance');

				});
			</script>

			<div class="alert alert-info">
      	<strong>The file editor has been temporarily disabled due to issues in saving. Please edit files locally and upload.</strong>
      </div>

			<div id="elfinder"></div>
			<pre class="cli" id="gitstatus"></pre>
			<div style="float: right;">
				<a class="btn btn-danger resetbutton">Reset</a>
				<a class="btn btn-success savebutton">Deploy Changes</a>
			</div>


		  </div>

			<script type="text/javascript">
				$('.resetbutton').click(function() {
					if( !confirm('ALL UNSAVED CHANGES WILL BE LOST. Are you sure?')) {
				    return false;
				  }
					$.get('?f=git-reset',function()
					{
						alert('Repository reset!');
						location.reload();
					}).fail(function() { alert("Unknown error resetting repository."); });
				});

				$('.savebutton').click(function() {
					$('#modalpleasewait').modal('show');
					$.get('?f=git-save',function(results){
						$("#gitstatus").html(results);
						$('#modalpleasewait').modal('hide');
					}).fail(function() {
						alert('There was an error saving your application. Try reset and try again.');
					});
				});

				function GitStatus() {
				  $.get('?f=git-status', function(results) {

				      $("#gitstatus").html(results);

				      if (results.indexOf('nothing to commit') >= 0) {
				        $('.savebutton').hide(500);
				      } else {
				        $('.savebutton').show(500);
				      }

				  });
				};

				$(document).ready(function() {
				  GitStatus();
				});

			</script>
<?php include( 'footer.php'); ?>
