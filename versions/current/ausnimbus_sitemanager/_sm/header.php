<?php require_once( 'auth.php'); ?>

<!DOCTYPE html>
<html lang="en" class="app">

<head>
  <meta charset='utf-8'>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>AusNimbus Site Manager</title>

  <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
  <link href='//cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css' rel='stylesheet'>
  <link href='//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-bounce.css' rel='stylesheet'>
  <link href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet'>
  <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='/console/assets/app.css' rel='stylesheet' type='text/css'>
  <!-- elfinder -->
  <link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" type="text/css" media="screen" href="//<?php echo $_SERVER['OPENSHIFT_APP_DNS']; ?>/_sm/lib/elfinder/css/elfinder.css">

  <!-- Javascript  -->
  <script src="/console/assets/console.js" type="text/javascript"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.0.7/jquery.browser.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.1/jquery.slimscroll.min.js' type='text/javascript'></script>
  <script src="/console/assets/fuelux.js" type="text/javascript"></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.0.5/parsley.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/Sortable/0.3.0/Sortable.min.js' type='text/javascript'></script>
  <script src="/console/assets/app.js" type="text/javascript"></script>
  <script src="/console/assets/app.plugin.js" type="text/javascript"></script>

  <!-- elfinder -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.10/tinymce.min.js"></script>
  <script src="//<?php echo $_SERVER['OPENSHIFT_APP_DNS']; ?>/_sm/lib/elfinder/js/elfinder.min.js"></script>

  <style>
    #elfinder .elfinder-button {
      -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box
    }

    .elfinder-toolbar {	border:	1px solid #ccc !important; }
    .elfinder-cwd-view-icons .elfinder-cwd-file .elfinder-cwd-filename.ui-state-hover,
    .elfinder-cwd table td.ui-state-hover,
    .elfinder-button-menu .ui-state-hover {
      color: #000 !important;
    }

    td {
      vertical-align: middle !important;
    }

    .aside-md {
      width: initial !important;
    }
  </style>
</head>

<body>

  <section class='vbox'>
    <header class='bg-dark dk header navbar navbar-fixed-top-xs'>
      <div class='navbar-header aside-md'>
        <a class='navbar-brand' href='/'>
          <img class='m-r-sm' style="display: inline" src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAgCAYAAABgrToAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHl%20wZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTc1QTk0MjM0MTdBMTFFNEFCRjhBNDdDNEY2QzZDODMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTc1QTk0MjQ0MTdBMTFFNEFCRjhBNDdDNEY2QzZDODMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBNzVBOTQyMTQxN0ExMUU0QUJGOEE0N0M0RjZDNkM4MyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBNzVBOTQyMjQxN0ExMUU0QUJGOEE0N0M0RjZDNkM4MyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PrFP5pkAAAjfSURBVHjazFcLUJTXFT77+Pf9Xt4LK29BwLCIAj7wRcRHNDYmvoJRcaYmUjuKnUxiZ0ynptjaxhgVmxjRaDW2scnUmNQY3wkqoPgAxSIIssAu7C7s+/34e/51cDCKEsp0vDN3/53733Pvd8/5znfuTxtR+A4MsTGwv4K9AHss0GgCfNr8LkurXXPrtOnexa9Ivy/wZFMaRE9+Cx8MMDaeBbv29oCbMIcIrhj7duzCR7elAyEMh5CMuNWi+PHm3oaTJVb1tcPwPzT6ELx2BnsFkAEhjc4Egi8HljAMmBwRMNh8YLD4QOIEgh8ijsorPhQ+7vVvgUYfMsCf68HzAOREkiSBJQgBDCFY26/XOQ33jnlt+tuIzEfwZQlsmXI6PzxlOk0UxpAk5c9mMDkXNJcqJvdzNXbqhxxWgLuwT6T+sEWR4DS0aLuvfL7ca+851X8SjgO0Xd1qYBAyedrMv0qTp%20y4UJ07K91h15Yb64yV99uhhcJs1wxbiHOwlJBkAQhAGNk1dXcf5nSk/Bde/kX5vr6Hu+CJt1YFVPqcRZKkFa9gSBbUOCBQZwORJgfR5ns0pSeLEwQDcjF3F5IoAQ2nTXqyYRAb8ur6X0rgMiM6eARGjJyPdGODs7QLqMFTzWLqv+5xmvTRl+hyPWStw6pu/DM9eggfwgM9hArepA9c0DD3EyDMCN51JEZ3OZIOp6cLmgM/dRr1j8UWQWfQeRGUVYHJwgC2Uwr3Th8DrsIC5vRECyFGqWVqrdpuaMouEsdmLWRKFmU5w5H63lSQE8naOPPa8Q9/8DXqcHBxAklyJgH6Ji6QjIgJ3weORoTQGC3x2I+noavyib+q4Nz8ExdiZYGytw4P4GSyBROA06WxkIOD/6bKm5soyxaTVxzHrV/udFqATfKBxmCBNKSgVRGeajY1nSi33a/YNyEEMSQYC07NE4fsYHGGu124UuHrusz0WbSRyzUlCADx2Q73PZblPzU97ZR1EqqZBT1NtOoL7Nw51eB1WQ1hqniZj4dvnI1XT5/bfyGPWXAx4nHaPRQ8uU6fV7zQ5fQ4joLADLzRBHDP11xWR44sv0Zgs3iPaSlUS5JOK4EmuUV7CcFRb7l/5k9vUeZkk/T2Ul1HvBKhvEcBgcn32npqw1FyYUPopxbVVfo9r7wPJCB4SGAQHOCI5EDwhdFz57vy1A5te9thMFjrBxcDGLvBauv7jdRhvUMlMZ/FiGSyelCuPnSaKy1srTZ4cgXs3qk//RYWYnEGAyhlvA5MraUXSxmovf7YcvXbwaZyUJWRCbskOoDOYB1xm/RtUUjzOkgBiZoBEmUJxsevCH4tUbitmDra4/AWZMTmzZ1WVr/vI47A6+tuhZm5T5K9Zj+XvhKZyz+xgiOlMzq9oJEmBe3UgcARPBCMm/ALGrHwfJpbujUZq3nWaup8ILnjqYOUgkZv1II4ZGTGmuOxhueuqr7RFZBaULfi%20syS6OHpn7CE+bfihtP7t9nTRl2iyhMuuNoMzI0+fsNt49125V1254JHtYXFBkF0LCtCWQ/mopxE1eCKKoxPkuk67a6zDL0YPP1CYanQ4uswFCU8bG2Q2dWrP6Tq3PZe9t+Lp8G1cakasq2vS+y6Q3m9puV/XZoOxUMzniccIYVZGp+cdtDHHChD+Ym38sx0So7puU9OJyUC3/HSRMLwKxMhX8Xjc4DJrNKB+76QTrQa0KBKCPe0+VKQw3JhHIk7JeRIBtIcnZddFjCj0eW+94iTJ1jDJv3szwUXkr7Hr1dYehMyhfLkPLGXH8+LUY0RpaZvFH99Xn9r7T21r/d+qlatl7kDJvDVg198BloRQGOBjKk2yRLJ/gCtEjerwY8FDrrIDegIHC3L8FfF7USBnWbzG4Lb1ujiSM7nVaCUovOeIQ4IcqqUQF3Z0qXe2+jbNNbQ214WOXHMTy2EWXREVZZAkZwRJEhTGpcAVKx7UgEAxjoSBshE0YEZuPpwvc+uLPFd9vnJVw83DZRp4sEhcPfSjGT62nKKcUGEePFsF62FZtC0FwUAdpNH9HzYkbDf/aea6z9nu9MDI+jC2SZwVDbeyoQvXgMLW1Jz/PKdm1xWnq0bCEsh3IFTqDYE3FydtRQpJ0DZedrRf+8XvtjXPlaGeljNWXj21xWwxteWt3HZbGpgFuCH6Pm9owGFL0UNAzyC9A4YYgX/FdwO/F/wRQNu3V3zZcrXh3KUbh5gO+Mnj4w8OKYmByhMCRjdC4jOqwIIlGzV+7L21B6Uq/24GuDgBKAmiunzK2XTy2ATNx/0CeEUYljEl56c0KRVbBCz6PC5zoITYCQ3q0dVw5cRY5lhE+Oj87gO8YbB5wETiTK4DGbz65ePNI2TRKv5+0blj2IkAOztdW7o16yHIMVyKebD7W1BBjW8Mpu059ZrD3MPR2TtaKzV9KRoxSOAwd8MPWFeluS8/t+CmLlyGnD5JIZJtODcitZvWlYx931%20V34YKC1pMlTQD56LngtuiWdlXt6YbhaYsGy3yw52k1OWL+nJcjnKYvh9a96vpu7s5pUjn95G43BVDxdkhgQqloAKUWfQlrxEVpk3srFOBzKHC6ANn17fVBs1Xe0XGk4qsGmWcivwppPNvy2+1Zl2bPsJYmTkHdKSqyprOKa7p67hMP6YfOgODo57rWDLWRS4coDkZlT45b+U0cmFBQdGowt5T3q+2ZYvuqojKShbGC6PhzDbAevy673OMyAPBzZdHJ/69Fl8Wk+t7NtMGuSj9/OhgaQykJ5YhZQ173+i1IAmRy+z2s3A08WMZIa8zptDcMRmUED5Moi8A64Pnhr9uKF88GnWV+l8GCVkCqpUPFDYyRYszMtmuYb/1eAlPD2NF0Fp7H7sfJGVQksV2xZ/Av4fZKOpbJka83H62fA89b4IYq07FVbalec8JCFW0428eRRsfA8NrwDzpm7o8q17GsLmbe2vEaeqBr3POJk5by1vbr4NEkuPaojFdkzVz+X3kydV7L/tb+1kIuOtJMiRVLOcwkSK0t6TO5L74qikyf+XNv/CjAAEp71DnXJsgEAAAAASUVORK5CYII='> AusNimbus Site Manager</a>
      </div>

      <ul class='nav navbar-nav hidden-xs' style='float: right;'>
        <li class='dropdown'>
          <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
            <span class='thumb-sm avatar pull-left'>
              <img src='https://www.gravatar.com/avatar/<?php echo md5($_SERVER[' HTTP_X_REMOTE_USER ']); ?>=128'>
            </span>
            <?php echo $_SERVER[ 'HTTP_X_REMOTE_USER']; ?>
            <span class="fa fa-angle-down"></span>
          </a>

          <ul class='dropdown-menu animated fadeInRight'>
            <li style="list-style: none">
              <span class='arrow top'></span>
            </li>

            <li>
              <a href='/console/settings/'>Settings</a>
            </li>

            <li>
              <a href='/account/clientarea?action=details'>Profile</a>
            </li>

            <li>
              <a href='/account/knowledgebase'>Help</a>
            </li>

            <li class='divider'></li>

            <li>
              <a href='/logout/'>Logout</a>
            </li>
          </ul>
        </li>
      </ul>
    </header>

    <section>
      <section class='hbox stretch'>
        <!-- .aside -->
        <section id='content'>
          <section class='vbox'>
            <section class='scrollable padder'>
              <div style="margin-top: 30px;"></div>
              <div class="alert alert-info">
                <strong>THIS IS STILL A BETA RELEASE. PLEASE REPORT ALL BUGS OR FEATURE REQUESTS.</strong>
              </div>

              <?php if (empty($users)) { ?>
              <div class='col-lg-12'>
                <section class='panel panel-default portlet-item'>

                  <header class="panel-heading">
                    <h4 style="display: inline;">
                      <?php echo $header; ?>
                    </h4>
                  </header>
                  <section class="panel-body">
                    <?php } else { ?>
                    <div class="col-lg-2">
                      <section class='panel panel-default portlet-item'>

                        <header class="panel-heading">
                          <h4 style="display: inline;">
                            Navigation
                          </h4>
                        </header>
                        <section class="panel-body" style="padding: 0 !important;">
                          <div class="list-group bg-white">
                            <a href="?f=dashboard" class="list-group-item"><i class="fa fa-chevron-right icon-muted"></i>
																<i class="fa fa-dashboard icon-muted fa-fw"></i>
																Dashboard
															</a><a href="?f=files" class="list-group-item"><i class="fa fa-chevron-right icon-muted"></i>
																<i class="fa fa-files-o icon-muted fa-fw"></i>
																File Manager
															</a><a href="?f=db" class="list-group-item"><i class="fa fa-chevron-right icon-muted"></i>
																<i class="fa fa-database icon-muted fa-fw"></i>
																Database Manager
																</a></div>
                        </section>
                      </section>
                    </div>

                    <div class="col-lg-10">
                      <section class='panel panel-default portlet-item'>

                        <header class="panel-heading">
                          <h4 style="display: inline;">
                            <?php echo $header; ?>
                          </h4>
                        </header>
                        <section class="panel-body">

                          <?php } ?>
