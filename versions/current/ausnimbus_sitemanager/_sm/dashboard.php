<?php $header='Dashboard' ; include( 'header.php'); ?>

<h3 class="text-center m-none">
  Welcome to AusNimbus Site Manager!
</h3>

<p class="text-center m-b-lg">
  This tiny place will let you have access to all the goodies which command line wizards have, such as cloud scaling and application backup support.
  <br /> From here you can upload your files through an easy to use web file manager and get access to your database through a graphical editor.
</p>

<div class="alert alert-success commandcomplete" style="display: none;">

</div>

<div class="col-lg-6">
  <div class="panel-group m-b" id="accordion2">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapse21" >
          <h5>Enable Hot Deploy</h5>
        </a>
      </div>
      <div class="panel-collapse collapse" id="collapse21" >
        <div class="panel-body">
          <p>
						Hot deploying your application enables you to upload changes to your site without shutting down the web server while changes are made, thus increasing deployment speed and minimizing application downtime. Some case scenarios may require the shutdown to load new dependencies.
          </p>
					<p>
						<strong>To disable hot_deploy, remove the file .openshift/markers/hot_deploy</strong>
					</p>
          <footer class="wrapper text-right bg-light lter">
            <a class="btn btn-s-md btn-success hot_deploy">Enable Hot Deploy</a>
          </footer>
        </div>
      </div>
    </div>


		<div class="panel panel-default">
      <div class="panel-heading">
        <a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapse22" >
          <h5>Setup Automated Backup</h5>
        </a>
      </div>
      <div class="panel-collapse collapse" id="collapse22" >
        <div class="panel-body">
          <p>
            <strong>This feature is not yet implemented, but is planned for next release.</strong>
          </p>
          <footer class="wrapper text-right bg-light lter">
            <a class="btn btn-s-md btn-info">Configure Automated Backup</a>
          </footer>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="col-lg-6">
  <div class="panel-group m-b" id="accordion3">


    <div class="panel panel-default">
      <div class="panel-heading">
        <a class="accordion-toggle" data-parent="#accordion3" data-toggle="collapse" href="#collapse31" >
          <h5>Backup Application</h5>
        </a>
      </div>
      <div class="panel-collapse collapse" id="collapse31" >
        <div class="panel-body">
          <p>
            This process can take an instant snapshot of your current application and database. The backup is stored as a .tar.gz file which can be restored in a similar method.<br />
						You can also find and download this backup file in the File Manager -> Backup section.
          </p>
					<p>
						<strong>Please note, your application is stopped during this process to prevent possible data corruption.</strong>
					</p>
          <footer class="wrapper text-right bg-light lter">
            <a class="btn btn-s-md btn-danger backup_app">Backup Application</a>
          </footer>
        </div>
      </div>
    </div>


		<div class="panel panel-default">
      <div class="panel-heading">
        <a class="accordion-toggle" data-parent="#accordion3" data-toggle="collapse" href="#collapse32" >
          <h5>Restore Application</h5>
        </a>
      </div>
      <div class="panel-collapse collapse" id="collapse32" >
        <div class="panel-body">
          <p>
            This process will restore your application to a previous snapshot state. This will only restore the application called LATEST.tar.gz which it looks for in the Backup folder.<br />
          	You can do this in the File Manager -> Backup section by either uploading an old snapshot called LATEST.tar.gz or renaming an existing backup.
					</p>
					<p>
						<strong>Please note, your application is stopped during this process to prevent possible data corruption. </strong>
						<strong>ALL PREVIOUS DATA WILL BE LOST!</strong>
					</p>
          <footer class="wrapper text-right bg-light lter">
            <a class="btn btn-s-md btn-danger restore_app">Restore Application</a>
          </footer>
        </div>
      </div>
    </div>


  </div>
</div>

<script>
$('.hot_deploy').click(function() {
  $('#modalpleasewait').modal('show');
  $('.commandcomplete').fadeIn(500);

  $.get('?f=hot_deploy',function(results){
    $(".commandcomplete").html(results);
    $('#modalpleasewait').modal('hide');
  });
});

$('.backup_app').click(function() {
  if( !confirm('Your application will be stopped while we take a snspshot. Are you sure?')) {
    return false;
  }

  $('#modalpleasewait').modal('show');
  $('.commandcomplete').fadeIn(500);

  $.get('?f=backup',function(results){
    $(".commandcomplete").html(results);
    $('#modalpleasewait').modal('hide');
  }).fail(function() {
    alert('There was an error backing up your application.');
  });
});

$('.restore_app').click(function() {
  if( !confirm('ALL PREVIOUS DATA WILL BE LOST. Are you sure?')) {
    return false;
  }

  $('#modalpleasewait').modal('show');
  $('.commandcomplete').fadeIn(500);

  $.get('?f=restore',function(results){
    $(".commandcomplete").html(results);
    $('#modalpleasewait').modal('hide');
  }).fail(function() {
    alert('There was an error backing up your application.');
  });
});
</script>

<?php include( 'footer.php'); ?>
