<?php

require_once('auth.php');
require_once('lib/git/Git.php');

$repo = Git::open($_SERVER["OPENSHIFT_AUSNIMBUS_SM_DIR"].'/repo');

$currentDate = date("Y-m-d");
$currentTime = date("H:i:s");
$currentDate =  date("Y-m-d H:i:s", strtotime($currentDate . $currentTime));

echo "Files added/modified: <br />";
echo $repo->add('.');

$repo->commit($currentDate.' commit made by AusNimbus Site Manager',true);
echo $repo->push('origin', 'master');

echo "<br />Current Status:<br />";
echo "<pre>".$repo->add('.')."</pre>";

?>
