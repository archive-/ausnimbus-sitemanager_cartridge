<?php

require_once('auth.php');
require_once('lib/git/Git.php');

$filename = $_SERVER["OPENSHIFT_AUSNIMBUS_SM_DIR"].'/repo/.openshift/markers/hot_deploy';

if (file_exists($filename)) {
    echo "hot_deploy already enabled.";
} else {
  if (touch($filename)) {
    $repo = Git::open($_SERVER["OPENSHIFT_AUSNIMBUS_SM_DIR"].'/repo');

    $currentDate = date("Y-m-d");
    $currentTime = date("H:i:s");
    $currentDate =  date("Y-m-d H:i:s", strtotime($currentDate . $currentTime));
    echo "<pre>".$repo->add('.')."</pre>";
    $repo->commit($currentDate.' hot_deploy commit made by AusNimbus Site Manager',true);
    $repo->push('origin', 'master');
  } else { echo 'failed to add hot_deploy'; }
}

?>
