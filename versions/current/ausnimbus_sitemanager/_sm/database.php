<?php
	$header = 'Database Manager';
	include('header.php');

							if ( isset($_SERVER['OPENSHIFT_MYSQL_DB_HOST']) )
							{
								$DB_HOST = $_SERVER['OPENSHIFT_MYSQL_DB_HOST'];
								$DB_PORT = $_SERVER['OPENSHIFT_MYSQL_DB_PORT'];
								$DB_USER = $_SERVER['OPENSHIFT_MYSQL_DB_USERNAME'];
								$DB_PASS = $_SERVER['OPENSHIFT_MYSQL_DB_PASSWORD'];
								$type = 'server';
								
								echo "<b>DB Driver:</b> MYSQL <br />";
								echo "<b>DB Host:</b> $DB_HOST <br />";
								echo "<b>DB Port:</b> $DB_PORT <br />";
								echo "<b>DB User:</b> $DB_USER <br />";
								echo "<b>DB Pass:</b> $DB_PASS <br /><br />";

							} else if ( isset($_SERVER['OPENSHIFT_POSTGRESQL_DB_HOST']) )
							{
								$DB_HOST = $_SERVER['OPENSHIFT_POSTGRESQL_DB_HOST'];
								$DB_PORT = $_SERVER['OPENSHIFT_POSTGRESQL_DB_PORT'];
								$DB_USER = $_SERVER['OPENSHIFT_POSTGRESQL_DB_USERNAME'];
								$DB_PASS = $_SERVER['OPENSHIFT_POSTGRESQL_DB_PASSWORD'];
								$type = 'pgsl';
								echo "<b>DB Driver:</b> POSTGRESQL <br />";
								echo "<b>DB Host:</b> $DB_HOST <br />";
								echo "<b>DB Port:</b> $DB_PORT <br />";
								echo "<b>DB User:</b> $DB_USER <br />";
								echo "<b>DB Pass:</b> $DB_PASS <br /><br />";

							} else if ( isset($_SERVER['OPENSHIFT_MONGODB_DB_HOST']) )
							{
								$DB_HOST = $_SERVER['OPENSHIFT_MONGODB_DB_HOST'];
								$DB_PORT = $_SERVER['OPENSHIFT_MONGODB_DB_PORT'];
								$DB_USER = $_SERVER['OPENSHIFT_MONGODB_DB_USERNAME'];
								$DB_PASS = $_SERVER['OPENSHIFT_MONGODB_DB_PASSWORD'];
								$type = 'mongo';

								echo "<b>DB Driver:</b> MONGODB <br />";
								echo "<b>DB Host:</b> $DB_HOST <br />";
								echo "<b>DB Port:</b> $DB_PORT <br />";
								echo "<b>DB User:</b> $DB_USER <br />";
								echo "<b>DB Pass:</b> $DB_PASS <br /><br />";

							} else {
								$DB_HOST = '';
								$DB_PORT = '';
								$DB_USER = '';
								$DB_PASS = '';
								echo "<h4 style='text-align: center;'><b>No Database Driver Installed</b></h4>";
								echo "<p>If you have added a database recently, try restarting your application from the portal.</p><br />";
							}

							?>

							<a href="https://<?php echo $_SERVER['OPENSHIFT_APP_DNS']; ?>/_sm/lib/adminer/adminer.php?<?php echo $type; ?>=<?php echo "$DB_HOST:$DB_PORT"; ?>&username=<?php echo $DB_USER; ?>" class="btn btn-success" target="_blank">Load Adminer</a>
							<p>
								<br />
								<b>Note:</b> Copy the password before loading adminer.<br />
								<b>Tip:</b> If you have set your application to scaling, the database server can be accessed remotely.
							</p>

<?php include('footer.php'); ?>

<script>
$('.reload').click(function() {
  $.get('?f=reload',function(results){
    $('#modalpleasewait').modal('show');
		setTimeout(location.reload, 300000);
  });
});
</script>
