<?php

	require_once('auth.php');

	# Limited function set
	$f = $_GET['f'];

	switch ($f) {

		case 'dashboard':
			include('dashboard.php');
			break;

		case 'files':
			include('file.php');
			break;

		case 'db':
			include('database.php');
			break;


		# Functions
		case 'git-reset':
			include('f/hardreset.php');
			break;

		case 'git-save':
			include('f/save.php');
			break;

		case 'git-status':
			include('f/status.php');
			break;

		case 'hot_deploy':
			include('f/hot_deploy.php');
			break;

		case 'backup':
			include('f/backup.php');
			break;

		case 'restore':
			include('f/restore.php');
			break;

		case 'connector':
			include('lib/elfinder/php/connector.php');
			break;

		default:
			include('dashboard.php');
			break;
	}

?>
